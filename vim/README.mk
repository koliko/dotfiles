Cuando se realice una nueva instalación hay que efectuar los siguientes pasos:

1. Instalar janus

    curl -Lo- https://bit.ly/janus-bootstrap | bash

2. Copiar los directorios y los archivos:

   mv colors ~/.vim
	 mv janus ~/.janus 
	 mv vimrc ~/.vimrc
	 mv vimrc.after ~/.vimrc.after

# Plugins que tengo que instalar

- NerdTRee
- vim-rails
- vim-bundler
- vim-ruby
- vim-plastic-markdown
